dataset = []
dataset.append(input())

for i in range(int(dataset[0])):
    dataset.append(input())

k = 4
d = 3

line = []

for i in range(int(dataset[0])//d):
    val = int(dataset [1+i*d])
    for j in range(d):
        if val < int(dataset[1+i*d+j]): 
            val = int(dataset[1+i*d + j])
    line.append(val)

for i in range(1,int(dataset[0])//d):
    line[i]= (line[i-1]*k+line[i])/(k+1)

dif = []
for i in range(5,int(dataset[0])//d):
    dif.append(line[i]-line[i-1])

c = 0

for i in range(2,len(dif)):
    if dif[i-2]*dif[i-1] < 0 and dif[i-2]*dif[i]: 
        c+=1

if c == 0:
    print('linear')
elif c == 1:
    if abs(dif[len(dif)//4*3] - dif[len(dif)-3])>1: 
        print('quadratic')
    else: print('module')
elif c == 2:
    print('cubic')
else:
    print('sinus')
