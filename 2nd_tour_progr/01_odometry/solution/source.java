
import java.awt.geom.Line2D;
import java.io.File;
import java.io.FileNotFoundException;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    private static double life_cycle = 0;
    private static double time = 0;

    public static void main(String[] args) throws FileNotFoundException {
        
		// read input
        Scanner scan = new Scanner(System.in);
        ArrayList<Num> arrayOfSpeed = new ArrayList<>();
        ArrayList<Cordinate> arrayOfPoints = new ArrayList<>();
        life_cycle = Double.valueOf(scan.next().replace(',','.'));
        time = Double.valueOf(scan.next().replace(',','.'));
		
		//format control
        DecimalFormat df = new DecimalFormat("#.#");
        df.setRoundingMode(RoundingMode.HALF_DOWN);
        df.setMinimumFractionDigits(0);

        int counter = 0;
        arrayOfSpeed.add(new Num(0, 0));
        while (counter != life_cycle) {
            arrayOfSpeed.add(new Num(Double.valueOf(scan.next().
				replace(',','.')), Double.valueOf(scan.next().
				replace(',','.'))));
            counter++;
        }
        
        arrayOfPoints.add(new Cordinate(0, 0));

        double angle = 0;
        double A1; 
        double A2; 
        double b1; 
        double b2; 
        double xIntersect = 0;
        double yIntersect = 0;
        double distance = 0;
        boolean key = false;
        int interCount = 0;
		
        for (int i = 1; i < arrayOfSpeed.size(); i++) {
            angle = angle + arrayOfSpeed.get(i).angleSpeed * time;
            arrayOfPoints.add(
				new Cordinate(arrayOfPoints.get(i - 1).x + 
				arrayOfSpeed.get(i).lineSpeed * time * 
				Math.cos(angle),arrayOfPoints.get(i - 1).y + 
				arrayOfSpeed.get(i).lineSpeed * time * 
				Math.sin(angle)));

            if (arrayOfPoints.size() > 2) {
                for (int k = 1; k < arrayOfPoints.size() - 4; k++) {
                    for (int j = k + 2; j < arrayOfPoints.size() - 1; j++) {
                        key = Line2D.linesIntersect(
        						arrayOfPoints.get(k).x,
        						arrayOfPoints.get(k).y, 
        						arrayOfPoints.get(k + 1).x,
        						arrayOfPoints.get(k + 1).y,
        						arrayOfPoints.get(j).x,
        						arrayOfPoints.get(j).y,
        						arrayOfPoints.get(j + 1).x,
        						arrayOfPoints.get(j + 1).y);
                        if (key == true) {
                            interCount = k;
                            A1 = (arrayOfPoints.get(k).y - 
                                arrayOfPoints.get(k + 1).y) / 
                                (arrayOfPoints.get(k).x - 
                                arrayOfPoints.get(k + 1).x);
                            A2 = (arrayOfPoints.get(j).y - 
                                arrayOfPoints.get(j + 1).y) / 
                                (arrayOfPoints.get(j).x - 
                                arrayOfPoints.get(j + 1).x);
                            b1 = arrayOfPoints.get(k).y - 
                                A1 * arrayOfPoints.get(k).x;
                            b2 = arrayOfPoints.get(j).y - 
                                A2 * arrayOfPoints.get(j).x;
                            xIntersect = (b2 - b1) / (A1 - A2);
                            yIntersect = xIntersect * A1 + b1;

                            distance = Math.sqrt((xIntersect - 0) * 
                                (xIntersect - 0) + (yIntersect - 0) * 
                                (yIntersect - 0));
                            System.out.println((int) (distance));
                            return;
                        }
                    }
                }
            }
        }
    }
}

class Num {
    double lineSpeed;
    double angleSpeed;

    public Num(double lineSpeed, double angleSpeed) {
        this.angleSpeed = angleSpeed;
        this.lineSpeed = lineSpeed;
    }

    public double getLineSpeed() {
        return lineSpeed;
    }

    public double getAngleSpeed() {
        return angleSpeed;
    }
}

class Cordinate {
    double x;
    double y;

    public Cordinate(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }
}