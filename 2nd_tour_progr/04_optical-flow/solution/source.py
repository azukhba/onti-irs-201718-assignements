
import math
data = input().split()

m = int(data[0])
n = int(data[1])
a = float(data[2])
h = float(data[3])
countFrame = int(input())

file = ''
for i in range(countFrame*m):
    file +=input() +'\n'

lines = file.split('\n')

res = -1
nRes = 0
for r in range(countFrame - 1):
    if res != -1: break
    nRes = 0
    for i in range(m):
        if lines[i + m*r ].split() == lines[ m + m*r].split():
            c = 0
            for j in range(m - i):
                if lines[ i + m*r +j ].split() == lines[m + m*r + j]\
                        .split():
                    c = j + 1
                else:break
            if c == m - i:
                res = i
                nRes += 1
        if nRes > 1: res = -1;
print(round(2*math.tan(a*math.pi/180/2)*h*res/m*countFrame,2))