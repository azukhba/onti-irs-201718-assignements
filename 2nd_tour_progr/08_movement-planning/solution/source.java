
import com.sun.org.apache.xpath.internal.SourceTree;

import java.awt.*;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.io.*;
import java.util.*;
public class Main {
    public static Line2D.Double Povorot(Line2D.Double line, double grad){
        Line2D.Double new_line;
        double x1 = line.x1 , y1 = line.y1 ,x2 = line.x2, y2 = line.y2 , r ;
        grad = (Math.PI*grad)/180;
        r = Math.sqrt( (x2-x1) * (x2-x1) + (y2 - y1) * (y2 - y1));
        x2 = Math.cos(Math.acos((x2 - x1)/r) + grad) * r + x1;
        y2 = Math.sin(Math.asin((y2 - y1)/r) + grad) * r + y1;
        new_line = new Line2D.Double(x1,y1,x2,y2);
        return new_line;
    }
    public static double rastoyanie(Line2D.Double line, Point2D.Double point){
        return (Math.abs( (line.y2-line.y1) * point.x - (line.x2-line.x1) * 
					point.y + line.x2*line.y1 - line.y2*line.x1)/
                (Math.sqrt( (line.y2-line.y1)*(line.y2-line.y1) + (line.x2 - 
					line.x1)*(line.x2-line.x1))));
    }
    public static Point2D.Double Free_point(Point2D.Double[] arr, int koef,  
                Line2D.Double dotS, Line2D.Double dotE, Line2D.Double line){
        int k = 0;
        while(Intersection(arr,dotS,dotE,line)) {
            line = Povorot(line, koef);
        }
        Point2D.Double min = null;
        for (int i = 0; i < arr.length; i++)
            if (!(arr[i].x == dotS.x1 && arr[i].y == dotS.y1) && 
					!(arr[i].x == dotE.x1 && arr[i].y == dotE.y1))
                min = arr[i];
        for (int i = 1; i < arr.length && min != null; i++)
            if (rastoyanie(line,min) > rastoyanie(line,arr[i]) && 
					!(arr[i].x == dotS.x1 && arr[i].y == dotS.y1) && 
					!(arr[i].x == dotE.x1 && arr[i].y == dotE.y1))
                min = arr[i];
        return min;
    }
    public static boolean Intersection(Point2D.Double[] arr, Line2D.Double dotS, 
                                Line2D.Double dotE, Line2D.Double line) {
        for (int i = 0; i < arr.length; i++) {
            Line2D.Double edge = new Line2D.Double(arr[i].x, arr[i].y, 
                    arr[(i + 1) % arr.length].x, arr[(i + 1) % arr.length].y);
            if (edge.intersectsLine(line) && !edge.intersectsLine(dotS) 
						&& !edge.intersectsLine(dotE)) {
                return true;
            }
        }
        return false;
    }
    public static boolean IntersectionAll(Point2D.Double[][] arr,
            Line2D.Double dotS, Line2D.Double dotE, Line2D.Double line){
        boolean flag = false;
        for (int k = 0; k < arr.length && !flag; k++)
            if (Intersection(arr[k], dotS, dotE, line))
                flag = true;
        return flag;
    }

    public static double LenPath(String path){
        if (path.equals("")) return Double.MAX_VALUE;
        String[] s = path.split("\n");
        Point2D.Double[] Points = new Point2D.Double[s.length];
        double result = 0;
        for (int i = 0; i < s.length; i++){
            String[] s1 = s[i].split(" ");
            Points[i] = new Point2D.Double(Double.parseDouble(s1[0]), 
                                        Double.parseDouble(s1[1]));
        }
        for (int i = 1; i < Points.length; i++){
            result += Math.sqrt((Points[i-1].x - Points[i].x)*
                                (Points[i-1].x - Points[i].x) +
                    (Points[i-1].y - Points[i].y) * (Points[i-1].y - 
                                    Points[i].y));
        }
        return result;
    }
    public static boolean containPart(String s, String p){
        String[] arr = s.split("\n");
        for (int i = 0 ; i < arr.length; i++){
            if (arr[i].equals(p)) return true;
        }
        return false;
    }
    public static boolean polygon_check(Point2D.Double[] arr, 
                        Point2D.Double First, Point2D.Double Second){
        int j1 = 1000,j2 = 1000;
        boolean flag1 = false,flag2 = false;
        for (int i = 0; i < arr.length; i++){
            if (First.x == arr[i].x && First.y == arr[i].y) {
                j1 = i;
                flag1 = true;
            }
            if (Second.x == arr[i].x && Second.y == arr[i].y) {
                j2 = i;
                flag2 = true;
            }
        }
        if (flag1 && flag2){
            return Math.abs(j1-j2)==1 || Math.abs(j1-j2) == arr.length - 1;
        }
        return true;
    }
    public static void Path(Point2D.Double[][] arr,String path_lineS,
                            Point2D.Double lineS,Point2D.Double End) {
        String result = "";
       // System.out.println(lineS.x + " " + lineS.y );
        Line2D.Double dotS = new Line2D.Double(lineS.x,lineS.y,
													lineS.x,lineS.y);
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++){
                Point2D.Double lineE = arr[i][j];
                Line2D.Double line = new Line2D.Double(lineS.x, lineS.y, 
                                                        lineE.x, lineE.y);
                Line2D.Double dotE = new Line2D.Double(lineE.x,lineE.y,
                                                        lineE.x, lineE.y);
                boolean flag = true;
                for (int k = 0; k < arr.length && flag; k++)
                    if (Intersection(arr[k], dotS, dotE, line))
                        flag = false;
                if ((flag && !containPart(path_lineS,lineS.x + " " + lineS.y)) 
                                && polygon_check(arr[i],lineS,lineE)) {
                    if (path_point[i][j].split("\n").length > 
                            path_lineS.split("\n").length + 1 || 
                            (LenPath(path_point[i][j]) > 
                            LenPath(path_lineS + lineS.x + " " + 
                                                lineS.y + '\n') 
                            && path_point[i][j].split("\n").length == 
                            path_lineS.split("\n").length + 1  )
                            || path_lineS.equals("") || 
path_point[i][j].equals("")) {
                        path_point[i][j] = path_lineS + lineS.x + " " + 
                                                lineS.y + '\n';
                        Path(arr, path_point[i][j], 
                                new Point2D.Double(dotE.x1, dotE.y1), End);
                    }
                }
            }
        }
    }
    
    public static Point2D.Double IntersPoint(Line2D.Double line1,
                                                Line2D.Double line2){
        double k1,k2,b1,b2,y1,y2,x1,x2;
        y1 = line1.y1;y2 = line1.y2; x1 = line1.x1; x2 = line1.x2;
        k1 = (y2 - y1)/(x2 - x1);
        b1 = y1 - k1 * x1;
        System.out.println("k1: " + k1 + "   b1:" + b1);
        y1 = line2.y1;y2 = line2.y2; x1 = line2.x1; x2 = line2.x2;
        k2 = (y2 - y1)/(x2 - x1);
        b2 = y1 - k1 * x1;
        System.out.println("k2: " + k2 + "   b2:" + b2);
        return new Point2D.Double((b2-b1)/(k1-k2),(b2-b1)/(k1-k2) * k2 + b2);
    }
    
    public static Point2D.Double Check(Point2D.Double[][] arr,
                Point2D.Double lineS, Point2D.Double lineE, double a, double b){
        Line2D.Double lineStart = new Line2D.Double(lineS,lineE),
                                        lineEnd = new Line2D.Double(lineE,lineS);
        Point2D.Double pp;
        for (double i = 0; i < b; i+=0.1) {
            pp = new Point2D.Double(0,i);
            lineStart = new Line2D.Double(lineS,pp);
            lineEnd = new Line2D.Double(lineE, pp);
            if (!IntersectionAll(arr, new Line2D.Double(lineS, lineS), 
                                    new Line2D.Double(pp,pp), lineStart)
            && !IntersectionAll(arr, new Line2D.Double(lineEnd.getP1(), 
                    lineEnd.getP1()), new Line2D.Double(pp,pp), lineEnd)) {
                return pp;
            }
        }
        for (double i = 0; i < a; i+=0.1){
            pp = new Point2D.Double(i,0);
            lineStart = new Line2D.Double(lineS,pp);
            lineEnd = new Line2D.Double(lineE, pp);
            if (!IntersectionAll(arr, new Line2D.Double(lineS, lineS), 
                                    new Line2D.Double(pp,pp), lineStart)
                && !IntersectionAll(arr, new Line2D.Double(lineEnd.getP1(), 
                    lineEnd.getP1()), new Line2D.Double(pp,pp), lineEnd)) {
                return pp;
            }
        }
        lineStart = new Line2D.Double(lineS,lineE);lineEnd = 
                                        new Line2D.Double(lineE,lineS);
        for (double i = 0; i < a; i+=0.1){
            pp = new Point2D.Double(i,b);
            lineStart = new Line2D.Double(lineS,pp);
            lineEnd = new Line2D.Double(lineE, pp);
            if (!IntersectionAll(arr, new Line2D.Double(lineS, lineS), 
                                    new Line2D.Double(pp,pp), lineStart)
                    && !IntersectionAll(arr, new Line2D.Double(lineEnd.getP1(), 
                        lineEnd.getP1()), new Line2D.Double(pp,pp), lineEnd)) {
                return pp;
            }
        }
        lineStart = new Line2D.Double(lineS,lineE);lineEnd = 
                                        new Line2D.Double(lineE,lineS);
        for (double i = 0; i < b; i+=0.1){
            pp = new Point2D.Double(a,i);
            lineStart = new Line2D.Double(lineS,pp);
            lineEnd = new Line2D.Double(lineE, pp);
            if (!IntersectionAll(arr, new Line2D.Double(lineS, lineS), 
                                    new Line2D.Double(pp,pp), lineStart)
                    && !IntersectionAll(arr, new Line2D.Double(lineEnd.getP1(), 
                        lineEnd.getP1()), new Line2D.Double(pp,pp), lineEnd)) {
                return pp;
            }
        }
        return null;
    }
    public static String[][] path_point;
    public static void main(String[] args) throws Exception {
        Locale.setDefault(Locale.US);
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int a = in.nextInt(), b = in.nextInt();
        Point2D.Double Start = new Point2D.Double(in.nextDouble(),
															in.nextDouble());
        Point2D.Double End = new Point2D.Double(in.nextDouble(),
															in.nextDouble());
        Point2D.Double[][] arr = new Point2D.Double[n+1][];
        path_point = new String[arr.length][];
        for (int i = 0; i < n; i++){
            int m = in.nextInt();
            arr[i] = new Point2D.Double[m];
            path_point[i] = new String[m];
            for (int j = 0 ; j < m; j++) {
                arr[i][j] = new Point2D.Double(in.nextDouble(), 
															in.nextDouble());
                path_point[i][j] = "";
            }
        }
        arr[n] = new Point2D.Double[1];
        arr[n][0] = End;
        path_point[n] = new String[1];
        path_point[n][0] = "";
        Path(arr,"",Start,End);
        String del = path_point[n][0].split("\n")[0];
        path_point[n][0] = path_point[n][0].replaceFirst(del + "\n","");
        if (path_point[n][0].equals("")){
            System.out.println(0);
        }else {
            Point2D.Double pointcheck = Check(arr,Start,End,a,b);
            if (pointcheck == null || (pointcheck.x < 0 && pointcheck.x > a) 
                            || (pointcheck.y < 0 && pointcheck.y > b) ){
                System.out.println(path_point[n][0].split("\n").length);
                System.out.print(path_point[n][0]);
            }else{
                System.out.println(1);
                System.out.println(pointcheck.x + " " + pointcheck.y);
            }
         }
    }
}