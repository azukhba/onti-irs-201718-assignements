#include <iostream>
#include <vector>
#include <set>

using namespace std;

int address(int p1, int p2, int p3, int p4) {
	int data = p1;
	data <<= 8;
	data |= p2;
	data <<= 8;
	data |= p3;
	data <<= 8;
	data |= p4;
	return data;		
}

int read_ad() {
	int m1, m2, m3, m4;
	char c;
	cin >> m1 >> c >> m2 >> c >> m3 >> c >> m4;
	return address(m1, m2, m3, m4);
}	
	
set <int> s;

int main() {
	int mask = read_ad();
	int n;
	cin >> n;
	for (int i = 0; i < n; i++) {
		int x = read_ad();
		s.insert(mask & x);
	}
	cout << s.size(); 
	return 0;
}	