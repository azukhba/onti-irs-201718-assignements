#include <iostream>
#include <iomanip>
#include <vector>
#include <cmath>

using namespace std;

typedef long long ll;

struct Point {
	int x, y;
	Point() {}
	Point(int x, int y) : x(x), y(y) {}
};

Point operator-(Point &p1, Point &p2) {
	return Point(p2.x - p1.x, p2.y - p1.y);
}	

istream & operator >>(istream &is, Point &p) {
	is >> p.x >> p.y;
	return is;
}

ostream & operator <<(ostream &os, Point &p) {
	os << p.x << ' ' << p.y;
	return os;
}	

ll crossproduct(Point const &p1, Point const &p2) {
	return (ll)p1.x * p2.y - p2.x * p1.y;
}

int side(Point &p1, Point &p2, Point &p) {
	ll cross = crossproduct(p2 - p1, p - p1);
	if (cross == 0) return 0;
	if (cross > 0) return 1;
	else return -1;
}	

double dis(Point &p1, Point &p2) {
	return sqrt(((double)p2.x - p1.x) * (p2.x - p1.x) + 
	            ((double)p2.y - p1.y) * (p2.y - p1.y));
}	

int main() {
	vector <Point> tri(3);
	for (auto &i : tri) cin >> i;
	Point p;
	cin >> p;
	double ans = 0;
	for (int i = 0; i < 3; i++) {
	if (side(tri[i], tri[(i + 1) % 3], p) * 
	    side(tri[i], tri[(i + 1) % 3], tri[(i + 2) % 3]) == -1)
		ans += dis(tri[i], tri[(i + 1) % 3]);
	}
	cout << fixed << setprecision(15) << ans;	
    return 0;
}