def generate():
    return []

def check(reply, clue):
    a = float(reply)
    b = float(clue)
    return abs(a - b) / max(1.0, b) <= 1e-6

from collections import namedtuple
from math import sqrt

Point = namedtuple("Point", ["x", "y"])

# расстояние между точками a и b
def dist(a, b):
        return sqrt((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y))

# расстояние между точкой с и отрезком ab
def dist2(a, b, c):
        A = a.y - b.y
        B = b.x - a.x
        C = b.y * a.x - a.y * b.x
        cc = B * c.x - A * c.y
        d = A * A + B * B
        p = Point((cc * B - C * A) / d, -(cc * A + C * B) / d)

        return dist(c, p) if dist(a, p) + dist(p, b) - dist(a, b) < 1e-7 else min(dist(c, a), dist(c, b))

def solve(dataset):
    ds = dataset.splitlines()

    s = ds[0].split()
    n = int(s[0])
    m = int(s[1])

    s = ds[1].split()
    p = []
    for i in range(n):
        p.append(Point(int(s[i * 2]), int(s[i * 2 + 1])))

    s = ds[2].split()
    pm = []
    for i in range(m):
        pm.append(Point(int(s[i * 2]), int(s[i * 2 + 1])))

    dist = [1e10] * m

    for i in range(n - 1):
        for j in range(m):
            dist[j] = min(dist[j], dist2(p[i], p[i + 1], pm[j]))

    ans = dist[m-1]
    for i in range(m - 1):
        ans = max(ans, dist[i])

    return str(ans)