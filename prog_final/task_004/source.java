import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.StringTokenizer;

import static java.lang.Math.min;
import static java.lang.Math.sqrt;

public class Main {
    // Уборка
    private FastScanner in;
    private PrintWriter out;

    class Point {
        double x, y;

        Point(double x, double y) {
            this.x = x;
            this.y = y;
        }
    }

    private double eps = 1e-7;

    // расстояние между точками a и b
    private double dist(Point a, Point b) {
        return sqrt((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y));
    }

    // расстояние между точкой с и отрезком ab
    private double dist(Point a, Point b, Point c) {
        double A = a.y - b.y, B = b.x - a.x, C = b.y * a.x - a.y * b.x;
        double cc = B * c.x - A * c.y, d = A * A + B * B;
        Point p = new Point((cc * B - C * A) / d, -(cc * A + C * B) / d);

        return dist(a, p) + dist(p, b) - dist(a, b) < eps ? 
                  dist(c, p) : 
                  min(dist(c, a), dist(c, b));
    }

    // траектории и радиусы роботов
    private int n, m; // количество роботов и количество точек в траектории каждого робота
    private double[] radius; // радиусы роботов
    private Point[][] p; // все точки всех траекторий

    // мусор
    private int k;
    private Point[] pk;

    // инициализация
    private void init() throws IOException {
        n = in.nextInt();
        m = in.nextInt();
        k = in.nextInt();

        radius = new double[n];
        p = new Point[n][m];
        pk = new Point[k];

        for (int i = 0; i < n; i++) {
            radius[i] = in.nextInt();

            for (int j = 0; j < m; j++)
                p[i][j] = new Point(in.nextInt(), in.nextInt());
        }

        for (int i = 0; i < k; i++)
            pk[i] = new Point(in.nextInt(), in.nextInt());
    }

    // Основа решения
    private void solve() throws IOException {
        boolean[][] can = new boolean[n][k];

        for (int i = 0; i < n; i++)
            for (int ki = 0; ki < k; ki++)
                for (int j = 0; j + 1 < m && !can[i][ki]; j++)
                    can[i][ki] = dist(p[i][j], p[i][j + 1], pk[ki]) - radius[i] < eps;

        int full = 1 << n, ans = n + 1;
        boolean ok;
        for (int f = 1; f < full; f++) {
            ok = true;
            for (int ki = 0; ki < k && ok; ki++) {
                ok = false;
                for (int i = 0; i < n && !ok; i++)
                    ok = ((1 << i) & f) > 0 && can[i][ki];
            }
            if (ok)
                ans = min(ans, cnt(f));
        }

        out.println(ans > n ? -1 : ans);
    }

    private int cnt(int f) {
        int cnt = 0;
        for (char c : Integer.toBinaryString(f).toCharArray())
            cnt += c == '1' ? 1 : 0;
        return cnt;
    }

    class FastScanner {
        StringTokenizer st;
        BufferedReader br;

        FastScanner(InputStream s) {
            br = new BufferedReader(new InputStreamReader(s));
        }

        String next() throws IOException {
            while (st == null || !st.hasMoreTokens())
                st = new StringTokenizer(br.readLine());
            return st.nextToken();
        }

        int nextInt() throws IOException {
            return Integer.parseInt(next());
        }
    }

    private void run() throws IOException {
        in = new FastScanner(System.in);
        out = new PrintWriter(System.out);

        init();
        solve();

        out.flush();
        out.close();
    }

    public static void main(String[] args) throws IOException {
        new Main().run();
    }
}